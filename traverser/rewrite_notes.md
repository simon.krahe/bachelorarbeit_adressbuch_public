- matching über jahresgrenzen hinaus
- line merging prozess optimieren
- hierarchisieren der daten, seitenstruktur integrieren
- seperierungsversuche der unterdaten
- tests bauen, mit mehr daten machen

# anforderungen line objekt
- datafields: previousLine, followingLine, virginText, editedText, coord info, bool disabled, page object referenz, log (string array), type
- getText() variable die entweder den virgin text oder eine bearbeitete version liefert
- getYear() holt das jahr von der Seite
- disableLine(), falls text vorherigem objekt zugeordnet wurde
- log() erlaubt logging
- tapeLines() klebt zusammen gehörende line objekte zusammen, deaktiviert eines davon und setzt entsprechend interne flags, dass der geklebte text abgerufen wird
- indentClassify() prüft ausschließlich aufgrund der einrÜckungsinformationen wie weit die zeile eingerückt ist. es wird noch nicht als typ klassifiziert, nur die einrückung selbst
- typeClassify() die normale zuordnung des typs abhängig von der position und evtl der vorherigen Zeile. einschließlich seitennummern.
    - hier sollen bereits anschlÜsse an vorherige zeilen ermöglicht werden.
    - ist die zeile ein name und davor kommt eine addresse, soll die reihnefolge der beiden objekte getauscht werden
- levenshteinMediate() passt den eigenen text den vorgaben an. hier zunächst first come first save, später potential für speichern des originals und gemeinsame anpassung am ende
- switch() ermöglicht den tausch der beiden objekte in der reihenfolge
- internalListToArray() geht rekursiv die linked list der objekte durch und spuckt ein array der aktuellen reihenfolge aus
- postProcessInformation(): texte wie namenszeile, addresse etc werden wenn möglich in subfelder aufgeteilt.
- done() platzhalter für spätere levenshtein anpassung

# page objekt referenz
- datafileds: year
- ermittelte dimensionen
- seitennummer im addressbuch? wenn korrekt klassifiziert

# pprint objekt
- kann tripel / sets mit objektreferenzen in menschlich lesbare texttripel umwandeln, indem auf den einzelnen objekten getText gecalled wird

# vorgehensreihenfolge
- einlesen der zeile, daraus initialisieren des zeilenelementes und wenn nötig, seitenelementes. seitenelement wird kontinuirlich angepasst
- korrektur der lesereihenfolge: links vor rechts ist etwas wichtiger als höhe
- tapeLines() aufrauf auf die line objekts
- indentClassify() aufruf des auf die line objects
- typeClassify aufruf der line objects
- levenshteinigung der firmennamen
- iterieren über alle elemente und bilden von semantischen tripeln (person, position, firma, jahr) oder (person, ort, jahr), jeweils mit referenzen! auf das objekt statt. außerdem bilden von listen, damit man personen querien kann
- postProcessInformation() auf alle line objects, um namen von titeln etc zu trennen
- levenshteinigung der namen über jahresgrenzen hinaus
- done auf alle line objects
- ausgabe als CSV
- ausgabe als netzwerk
- unit test: stimmen die getesteten personen noch?
- visualisierung