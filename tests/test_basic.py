# coding=utf8
homepath_win = "/home/stkra/bachelorarbeit"
homepath_linux = "/home/simon/addressbuch_traverse"
homepath = ""

import socket # https://pythontic.com/modules/socket/gethostname
if socket.gethostname() == "hex":
    homepath = homepath_win
else:
    homepath = homepath_linux

import pytest
import re
import difflib
from testbook import testbook

def read_file(file_path):
    with open(file_path, 'r') as file:
        file_content = file.read()
    return file_content

expected_output = read_file(homepath + '/tests/goldstandard_1932.txt')
#print(expected_output)

@pytest.fixture(scope='module')
def tb():
    with testbook(homepath + '/traverser/notebook.ipynb', execute=True) as tb:
        #with testbook('buchtest.ipynb', execute=True) as tb:
        yield tb

"""
class LineDiffException(Exception):
    Exception raised for errors in the input salary.

    Attributes:
    salary -- input salary which caused the error
    message -- explanation of the error
    

    def __init__(self, salary, message="Folgende Zeilen stimmten nicht mit dem Original überein:"):
        self.salary = salary
        self.message = message
        super().__init__(self.message)


    salary = int(input("Enter salary amount: "))
    if not 5000 < salary < 15000:
        raise SalaryNotInRangeError(salary)
"""

#@testbook('/home/simon/addressbuch_traverse/traverser/oo_addressbuch_traverse.ipynb', execute=True)
def test_func(tb):
    differentLines = 0
    func = tb.ref("printForTests")
    actual_output = func()
    #print(actual_output)
    differ = difflib.Differ()
    diff_lines = list(
        differ.compare(expected_output.splitlines(),
        actual_output.splitlines()))

    # Step 4: Print differences
    for line in diff_lines:
        if line.startswith('-') or line.startswith('+') and not re.match("^[+-]\s*$", line):
            print(line)
            differentLines += 1
    """
    
    unique_diff_lines = set()
    for line in diff_lines:
        if line.startswith('-') or line.startswith('+'):
            #unique_diff_lines.add(line[2:])
            unique_diff_lines.add(line)

    for line in unique_diff_lines:
        print(line)
    """

    # Step 5: Assert and report
    assert len(diff_lines) == 0, f"Test failed. {str(differentLines)} lines are different."