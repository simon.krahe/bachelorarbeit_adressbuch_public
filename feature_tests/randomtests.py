
import re

continue_string = "Aktiengesellschaft "
ag_sub = re.sub('\s{1}(a[ck]{1}tien\s*-?\s*g(\.|esel*(\.|sch\.|schaft))|a\.?\s*-?\s*g\.?\s*)(\s+|$)', "AG\2", continue_string.lower())
print(ag_sub + "weiter")


ag_sub = "ag d. baumwoll-spinnereien, webereien"
ag_sub = re.sub(r'\bd[\.,]?(\s*)\b', 'der ', ag_sub)  # d., der TODO regex verbessern, whitespace soll von davor reproduzierrt werden und nicht neu erzeugt
print(ag_sub)
