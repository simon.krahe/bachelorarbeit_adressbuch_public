import requests
import pprint

#https://chat.openai.com/c/0fa17492-5168-4c39-9938-77f471b3e323
def geocoding_request(query, language=None, country=None, key=None):
    query_encoded = requests.utils.quote(query)
    base_url = f"https://api.maptiler.com/geocoding/{query_encoded}.json"
    # Create a dictionary to store the parameters
    params = {}
    # Add optional parameters if provided
    if language:
        params['language'] = language
    if country:
        params['country'] = country
    if key:
        params['key'] = key
    # Make the GET request
    response = requests.get(base_url, params=params)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Print or process the response data here
        pprint.pprint(response.json())
    else:
        # Print an error message if the request was not successful
        print("Error:", response.status_code, response.text)

# Example usage
query = "Heinrichshof 6, Glachau"
#query = "Gütershi. Westf."
language = "de"  # Replace with your desired language code
country = "de"    # Replace with your desired country code
api_key = "ENTFERNT"  # Replace with your MapTiler API key

geocoding_request(query, language, country, api_key)

